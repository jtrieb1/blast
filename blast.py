import os, sys, subprocess, shlex
import blast.contacts as contacts
from os import listdir
from os.path import isfile, join

def usage():
    print "This is a mass automailer written in python/powershell."
    print "It assumes you are sending from a gmail-hosted account."
    print "When launched, it will prompt you for credentials."
    print
    print
    print "Usage:"
    print
    print "|    In the 'staging' directory, place exactly 3 files. One of them"
    print "|    must be an html file (as a template for the email), one must  "
    print "|    be a csv file containing the contact list with headings for "
    print "|    first name and email, and one must be a txt file outlining "
    print "|    rules for text replacement, in the following format."
    print
    print
    print "Replacement Scheme Format:"
    print
    print "%UNIQUESTRING%; Replacement"
    print "%ANOTHERSTRING%; Another replacement"
    print "%ETCETERA%; Etc."
    print
    print "Each column in the spreadsheet will dynamically replace the string"
    print "%DYNAMIC{X}% in the template file, where {X} is replaced by the"
    print "position number of the column. Make sure that email is in position zero, or column A."
    print
    print "Written by Jacob Triebwasser."

def main():

    sourcepath = "staging"
    files = ["staging/" + f for f in listdir(sourcepath) if isfile(join(sourcepath, f))]
    
    if len(files) != 3:
        usage()
        return

    def fileval(ext):
        return [f for f in files if os.path.splitext(f)[-1].lower() == ext]
    
    template = fileval('.html')
    contactfile = fileval('.csv')
    rsch = fileval('.txt')

    if len(template) != 1 or len(contactfile) != 1 or len(rsch) != 1:
        usage()
        return

    print "It is highly recommended that you send a small batch of emails to"
    print "your own accounts to test rendering before a final push."
    print
    print
    print "Enter 'From' Address:"
    print
    emailfrom = raw_input()
    print
    print "Enter email subject line:"
    subject = raw_input()
    print
    

    contacts.pipe_render(contactfile[0], "out.txt")

    subprocess.call(["powershell", ".\\blast\\blast.ps1", "-EmailSubject", "'"+ subject +"'", "-Contacts", "out.txt", "-template", "'" + template[0] + "'", "-RSchem", "'" + rsch[0] + "'", "-EmailFrom", "'" + emailfrom + "'"])
                
    os.remove("out.txt")

main()
