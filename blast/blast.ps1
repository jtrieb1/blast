﻿

# Define Constants here

param([String] $EmailSubject, [String] $Contacts, [String] $template, [String] $RSchem, [String] $EmailFrom)

$cred = Get-Credential

# Students Format: "studentemail@something.com; Student Name; Other info; ...", pass as path
# Rschem Format: "%UNIQUESTRING%; Replacement value", pass as path
# Template file should be HTML.


$SMTPServer = "smtp.gmail.com"
$SMTPPort = "587"


$Students = Get-Content -Path $Contacts -ReadCount 0
$Rep = Get-Content -Path $RSchem

$content = (Get-Content -Path $template -ReadCount 0) -join "`n"

foreach ($repl in $Rep) {
        $old, $new = $repl.Split(';')
        $content = $content -replace $old, $new
}

$inc = 0

foreach ($studentString in $Students) {
    $contentf = $content
    $sInfo = $studentString.Split(';')
    $inc += 1
    foreach ($i in $sInfo) {
        
        $ordinal = $sInfo.IndexOf($i)
        $contentf = $contentf -replace "%DYNAMIC$ordinal%", $i

    }
    $EmailBody = $contentf
    $EmailTo = $sInfo[0]

    Invoke-Expression -Command 'Send-MailMessage -From $EmailFrom -to $EmailTo -Subject $EmailSubject -Body $EmailBody -SmtpServer $SMTPServer -port $SMTPPort -UseSsl -Credential $cred -BodyAsHtml'
    Start-Sleep -m 1000
    if (!($inc%1000)) {
        Start-Sleep -m 90000
    }
    if (!($inc%100)) {
        Start-Sleep -m 8000
    }
    if (!($inc%10)) {
        Start-Sleep -m 1000
    }
}