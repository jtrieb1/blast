import numpy as np
import pandas as pd

def name_format(x):
    name = x.split()[0]
    name = name.lower()[0].upper() + name.lower()[1:]
    return name

def pipe_render(pathin, pathout):
    contacts = pd.read_csv(pathin, dtype=object)
    contacts = contacts.loc[:, ~contacts.columns.str.contains('^Unnamed')]
    contacts.dropna(subset=["Email"], inplace=True)
    contacts.fillna("", inplace=True)
    cst = ""
    with open(pathout, 'a') as o:
        for index, row in contacts.iterrows():
            try:
                try:
                    row["First Name"] = name_format(row["First Name"])
                except:
                    pass
                try:
                    row["Last Name"] = name_format(row["Last Name"])
                except:
                    pass
                cst = '; '.join(row.tolist()) + "\n"
                o.write(cst)
            except:
                raise Exception('Incorrect format for contact csv!')
    return

