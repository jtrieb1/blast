# README #

This is Blast, a small mass mailer for Windows written in Python and Powershell.

### What is this repository for? ###

* Sending a whole lot of templated HTML emails to a lot of people
* Kicks

### How do I get set up? ###

* Put an HTML file in the staging folder as a template for your email
* Put a csv file in the staging folder as a contacts list, which must contain an Email column as the first column. Save excel spreadsheet as .csv file. [Only handles one sheet at a time.]
* Put a txt file in the staging folder with rules for what you want replaced in your template. It's allowed to be blank. Just for convenience, really. Some people don't like digging through HTML.
* For rules on how to organize your templates, see the usage() function in the blast.py code.

### Who do I talk to? ###

* If you have any issues, contact me at jacob.triebwasser@gmail.com and I'll do my best to help out.